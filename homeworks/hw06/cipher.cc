#include "cipher.h"
#include "Ccipher.h"
#define UPPER_CASE(r) ((r) - ('a' - 'A'))

struct Cipher::CipherCheshire {
    string cipherText;
};

Cipher::Cipher()
{
    smile = new CipherCheshire;
    smile->cipherText = "abcdefghijklmnopqrstuvwxyz ";
}
Cipher::Cipher(string in)
{
    smile = new CipherCheshire;
    smile->cipherText = in;
}
string Cipher::encrypt(string raw)
{
    string retStr;
    cout << "Encrypting..." << endl;
    for(unsigned int i = 0; i < raw.size(); i++) {
        unsigned int pos;
        bool upper = false;
        if(raw[i] == ' ') {
            pos = 26;
        } else if(raw[i] >= 'a') {
            pos = raw[i] - 'a';            
        } else {
            pos = raw[i] - 'A';
            upper = 1;
        }
        if(upper) {
            retStr += UPPER_CASE(smile->cipherText[pos]);
        } else {
            retStr += smile->cipherText[pos];
        }
    }
    cout << "Done" << endl;

    return retStr;
}

string Cipher::decrypt(string enc)
{
    string retStr;
    cout << "Decrpyting..." << endl;
    // Fill in code here
    for (unsigned int i =0; i < enc.size(); i++){
        unsigned int pos;
        bool upper = false;
        
        if(enc[i] == ' ') {
            pos = 26;
        } else if(enc[i] >= 'a') {
            pos = enc[i] - 'a';
            pos += 1;   
        } else {
            pos = enc[i] - 'A';
            pos += 1;
            upper = 1;
        }
        if(upper) {
            retStr += UPPER_CASE(smile->cipherText[pos]); // bug somewhere here
        } else {
            retStr += smile->cipherText[pos];
        }  
    }
    cout << "Done" << endl;

    return retStr;
}




struct CaesarCipher::CaesarCipherCheshire : CipherCheshire {
     int rot;
};

CaesarCipher::CaesarCipher()
{
    // Fill in code here--------------- Is this the constructor? 
    CaesarSmile = new CaesarCipherCheshire;
    CaesarSmile->cipherText = "abcdefghijklmnopqrstuvwxyz ";
}

CaesarCipher::CaesarCipher(string in, int rot)
{
    CaesarSmile = new CaesarCipherCheshire;
    CaesarSmile->cipherText = in;
    CaesarSmile->rot = rot;

}

string CaesarCipher::encrypt(string raw)
{
    string retStr;
    cout << "Encrypting..." << endl;
    // Fill in code here
    int offset;
    for (unsigned int k = 0; k < CaesarSmile->cipherText.size(); k++){
        if (CaesarSmile->cipherText[k] == ' '){
            offset = k;
        }
    }   //offset = 13
    string returnStr;
    for(unsigned int i = 0; i < raw.size(); i++) {
        unsigned int pos;
        bool upper = false;
        if(raw[i] == ' ') {
            pos = 26 + offset;
            returnStr += CaesarSmile->cipherText[pos];
            continue; 
        } else if(raw[i] >= 'a') {
            pos = raw[i] - 'a';
            pos += CaesarSmile->rot;                //Adding the rot to the position
        } else {
            pos = raw[i] - 'A';
            pos += CaesarSmile->rot;                //Adding the rot to the position
            upper = 1;
        }
        if(upper) {
            returnStr += UPPER_CASE(CaesarSmile->cipherText[pos]);
        } else {
            returnStr += CaesarSmile->cipherText[pos];
        }
    }
    return returnStr;
    cout << "Done" << endl;
    return retStr;
}

string CaesarCipher::decrypt(string enc)
{
    string retStr;
    cout << "Decrpyting..." << endl;
    // Fill in code here
    int first = CaesarSmile->cipherText[0] - CaesarSmile->rot + 1;
        first -= 'a';
 
    for (unsigned int i =0; i < enc.size(); i++){
        unsigned int pos;
        bool upper = false;
        if(enc[i] == 0) {
            pos = 26;
            retStr += smile->cipherText[pos];
            continue;
        } else if(enc[i] >= 'a') {
            pos = enc[i] - 'a';
            // cout << "WTF??      " << enc[i] << pos+first << endl;
            pos += first;
        } else {
            pos = enc[i] - 'A';
            // cout << "WHY IS M NOT RIGHT?? : "  << int(enc[i]) << endl;

            pos += first;
            upper = 1;
        }
        if(upper) {
            if (pos > 26){
                pos = pos % 26;
                pos -= 1;
            }
            retStr += UPPER_CASE(smile->cipherText[pos]);
        } 
        else {
            if (pos >= 26){
                pos = pos % 26;

                pos -= 1;
            }
            retStr += smile->cipherText[pos];
        }  
    }    
    cout << "Done" << endl;
    return retStr;
}